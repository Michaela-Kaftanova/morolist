import React, {Component} from 'react';

class AddNewItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const a = event.target.value;
        this.setState({
            value: a
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = this.state.value;
        this.setState({
            value: ''
        });

        const items = this.props.items;

        // let x = nechť existuje proměnná x
        // cons x = konstanta, nejde změnit

        // controluje jesli data jiz existuji, pokud ano, if vrati return a funkce skonci
        // pokud ne funkce pokracuje a zavola se funkce addItem
        for (let i = 0; i < items.length; i = i + 1) {
            if (items[i].text === data) {
                return;
            }
        }
        if (data === "")
            return;
        this.props.addItem(data);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label htmlFor="newItem"
                       className="InputLabel">
                    Name:
                </label>
                <input type="text"
                       id="newItem"
                       name="newItem"
                       onChange={this.handleChange}
                       value={this.state.value}
                />

                <input className="SubmitButton"
                       type="submit"
                       value="Submit"/>
            </form>
        )
    }
}

export default AddNewItem;