import React, {Component} from 'react';
import Item from './Item'

class List extends Component {

    constructor(props) {
        super(props);
        this.state = props.items;
        this.change = this.change.bind(this);
    }

    change(item){
        this.props.changeItem(item);
    }

    render() {
        // map rozdeli pole na jednotlive itemy a provede co je ve funkci pro kazdy z nich
        const a = this.state.map(i => {
            return (
                <Item  item={i}
                       changeItem={(item) => this.change(item)}
                />
            );
        });
        return a;
    }

}

export default List;