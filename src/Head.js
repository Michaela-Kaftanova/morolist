import React, {Component} from 'react';

class Head extends Component {

    constructor(props) {
        super(props);

        this.state = {
            // které hodnotou a kreté referencí?
            // v itemu jen odakaz na item nebo je tam zkopirovany?
            item: this.props.item,
            value: this.props.item.text
        };
        this.handleChange = this.handleChange.bind(this);
        this.rename = this.rename.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    handleChange(event) {
        const a = event.target.value;
        this.setState({value: a});
    }

    rename() {
        let i = this.state.item;
        i.editing = false;
        i.text = this.state.value;

        this.setState({item: i});
    }

    toggle(){
        let i = this.state.item;
        i.editing = true;

        this.setState({item: i});
    }

    render() {

        // muzu si to ukladat cele do jedne promenne?, (mam dvakrat div className="Card")
        if (this.state.item.editing) {
            return (
                <div>
                    <input type="text"
                           value={this.state.value}
                           onChange={this.handleChange}
                           style={{width: "150px"}}/>

                    <button className="EditButton"
                            onClick={this.rename}>
                        Rename!
                    </button>
                </div>
            )
        }
        else {
            return (
                <div>
                    <div className="Text">
                        {this.state.item.text}
                    </div>
                    <button className="EditButton"
                            onClick={this.toggle}>
                        edit!
                    </button>
                </div>
            )

        }

    }

}
export default Head;