import React, {Component} from 'react';
import Head from './Head';

class Item extends Component {

    constructor(props) {
        super(props);
        this.state = props.item;

        // Proste to tu musi byt, aby se dalo volat napric komponentama
        this.handleRemove = this.handleRemove.bind(this);
        this.modifyItem = this.modifyItem.bind(this);
    }

    handleRemove() {
        let item = this.state;
        item.deleted = true;
        this.props.changeItem(item);
    }

    modifyItem() {
        let item = this.state;
        item.done = !item.done;
        this.props.changeItem(item);
    }

    render() {
        const state = this.state;
        if (state.deleted)
            return "";
        else {
            return (
                <div className="Card CardBorder"
                     key={state.iden}>

                    <Head item={this.state}/>
                    <button className="EditButton"
                            type="button"
                            style={{clear:"both"}}
                            onClick={this.handleRemove}>
                        x
                    </button>
                    <div className="Check">
                        <input type="checkbox"
                               onChange={this.modifyItem}/>
                        <DoneColor doneC={state.done} />
                    </div>
                </div>
            );
        }
    }
}

const DoneColor = ({doneC}) => {
    if(doneC){
        return(
            <span style={{color: 'green'}}>
                Done
            </span>
        )
    } else {
        return(
            <span style={{color: '#FFCC53'}}>
                Undone
            </span>
        )
    }
};

export default Item;
