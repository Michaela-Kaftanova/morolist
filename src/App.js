import React, {Component} from 'react';
import './App.css';
import AddNewItem from './AddNewItem';
import List from './List';

class App extends Component {
    constructor(props) {
        super(props);

        this.addItem = this.addItem.bind(this);
        this.countItems = this.countItems.bind(this);
        this.removeDone = this.removeDone.bind(this);

        // nastaví default data
        this.state = {
            newId: 2,
            data: [
                {
                    iden: 1,
                    text: 'Nakoupit',
                    done: false,
                    deleted: false,
                    editing: false
                }
            ]
        };
    }

    addItem(item) {
        // ulozi cely state
        const state = this.state;

        // vytahne ze state nove id
        const id = state.newId;
        state.newId++;

        // udela push do dat ze state
        state.data.push({
            iden: id,
            text: item,
            done: false,
            deleted: false,
            editing: false
        });

        // Nastavi state a zavola ZNOVU render()
        this.setState(state);
    }

    countItems(all) {
        const state = this.state;

        // spocita kolik ukolu je hotovych
        // all dava true, false
        let ret = 0;
        for (let i = 0; i < state.data.length; i++) {
            if (!state.data[i].deleted) {
                if (all || state.data[i].done)
                    ret++;
            }
        }
        return ret;
    }

    removeDone() {
        const state = this.state;

        for (let i = 0; i < state.data.length; i++) {
            if (!state.data[i].deleted && state.data[i].done) {
                this.state.data[i].deleted = true;
            }
        }

        this.setState(state);
    }

    changeItem(item){
        const state = this.state;

        const idx = state.data.indexOf(item);
        if (idx > -1){
            state.data[idx].text = item.text;
            state.data[idx].done = item.done;
            state.data[idx].deleted = item.deleted;
            state.data[idx].editing = item.editing;
        }

        this.setState(state);
    }

    render() {
        return (
            <div className="App">
                <header>
                    <h1>To Do List</h1>
                </header>
                <main>
                    <div className="FormArea">
                        <h2>Add new item:</h2>
                        <AddNewItem items={this.state.data}
                                    //AddNew se zobrazi v props, item se pak preda do funkce(callback)
                                    addItem={(item) => this.addItem(item)}/>
                    </div>
                    <div className="List">
                        <h2>Tasks list (done {this.countItems(false)} of {this.countItems(true)})</h2>
                        <List items={this.state.data}
                              changeItem={(item) => this.changeItem(item)}/>
                    </div>
                    <button className="EditButton"
                            onClick={this.removeDone}>
                        Remove all done
                    </button>
                </main>
            </div>
        );
    }
}

export default App;

